import React  from "react";
import '../Calculator.css';
import CalculatorNumberButton from './CalculatorNumberButton';

class Calculator extends React.Component{
    
    constructor(props) {
        super(props);
        
        this.state = {
          isOperand1Null: true,
          isResultCalculated: false,
          operand1: null,
          operand2: null,
          operator: null,
          result: 0
        };
    
    }

    onNumberClick = (number) => {

        if(this.state.isResultCalculated === true){
          this.onClear();
        }

        this.setState((previousState) => {
      
          if(previousState.isOperand1Null === true){
            
              return {
                operand1: previousState.operand1 === null ? number : (number === '.' && previousState.operand1.includes('.') === true) ? previousState.operand1 : previousState.operand1 === '0' ? number : previousState.operand1.concat(number)
              }
            
          } else {

              return {
                operand2: previousState.operand2 === null ? number : (number === '.' && previousState.operand2.includes('.') === true) ? previousState.operand2 : previousState.operand2 === '0' ? number : previousState.operand2.concat(number)
              }

          }
        });
    }
    
    onClear = () => {
    
        this.setState({
            isOperand1Null: true,
            isResultCalculated: false,  
            operand1: null,
            operand2: null,
            operator: null,
            result: 0
        });     
    }
    
    selectOperator = (selectedOperator) => {
    
        if(selectedOperator === '-' && this.state.isResultCalculated === true){
          this.onClear();
        }
    
        this.setState((previousState) => {
    
          if(previousState.operand1 !== null && previousState.operand1 !== '-' && previousState.operand1 !== '.'  && previousState.operand1 !== '-.' && previousState.operator === null){
    
            return {
              isOperand1Null: false,
              operator: selectedOperator
            };
    
          } 
          else if(previousState.operand1 !== null && previousState.operator !== null && previousState.operand2 === null && selectedOperator==='-'){
    
            return {
              operand2: '-'
            };
    
          } else if(previousState.operand1 === null && selectedOperator==='-'){
    
            return {
              operand1: '-'
            };
    
          }
        });
    }
    
    calculateResult = () => {
    
        if(this.state.operand2 !== '-' && this.state.operand2 !== null && this.state.operand2 !== '.' && this.state.operand2 !== '-.' && this.state.operand1 !== '-' && this.state.operand1 !== null){
    
          this.setState((previousState) => {
      
            let num1 = parseFloat(previousState.operand1);
            let num2 = parseFloat(previousState.operand2);
            let calculatedResult = 0;
            let selectedOperator = previousState.operator;
      
            switch(selectedOperator){
              case '+': 
                calculatedResult = num1 + num2;
                break;
              case '-': 
                calculatedResult = num1 - num2;
                break;
              case '×': 
                calculatedResult = num1 * num2;
                break;
              case '/': 
                calculatedResult = num1 / num2;
                break;
              default:
                calculatedResult = 0;
            }
      
            return {
              isResultCalculated: true,
              result: calculatedResult
            };
      
          });
        }
    
    }

    render() {

        return (
          <div className="mainContainer">
    
            <h1>CALCULATOR</h1>
    
            <div className="calculatorContainer">
    
              <div className="output">
                <label>{this.state.operand1}</label>
                <label>{this.state.operator}</label>
                <label>{this.state.operand2}</label>
                <label className="result">Result= {this.state.result}</label>
              </div>
    
              <div className='input'>
                <div className='inputSection'>
                  <button onClick={this.onClear}>clear</button>
                  <div className="combinedButtons">
                    <CalculatorNumberButton 
                      function={this.onNumberClick}
                      value={'0'}
                    />
                    <CalculatorNumberButton 
                      function={this.onNumberClick}
                      value={'.'}
                    />
                  </div>
                  <button className = "operatorButton" onClick={this.calculateResult}>=</button>
                  <button className = "operatorButton" onClick={() => {
                    this.selectOperator('+')
                  }}>+</button>
                </div>
    
                <div className='inputSection'>
                  <CalculatorNumberButton 
                    function={this.onNumberClick}
                    value={'7'}
                  />
                  <CalculatorNumberButton 
                    function={this.onNumberClick}
                    value={'8'}
                  />
                  <CalculatorNumberButton 
                    function={this.onNumberClick}
                    value={'9'}
                  />
                  <button className = "operatorButton" onClick={() => {
                    this.selectOperator('-')
                  }}>-</button>
                </div>
    
                <div className='inputSection'>
                  <CalculatorNumberButton 
                    function={this.onNumberClick}
                    value={'4'}
                  />
                  <CalculatorNumberButton 
                    function={this.onNumberClick}
                    value={'5'}
                  />
                  <CalculatorNumberButton 
                    function={this.onNumberClick}
                    value={'6'}
                  />
                  <button className = "operatorButton" onClick={() => {
                    this.selectOperator('×')
                  }}>×</button>
                </div>
    
                <div className='inputSection'>
                  <CalculatorNumberButton 
                    function={this.onNumberClick}
                    value={'1'}
                  />
                  <CalculatorNumberButton 
                    function={this.onNumberClick}
                    value={'2'}
                  />
                  <CalculatorNumberButton 
                    function={this.onNumberClick}
                    value={'3'}
                  />
                  <button className = "operatorButton" onClick={() => {
                    this.selectOperator('/')
                  }}>÷</button>
                </div>
    
              </div>
            </div>
          </div>
        );
    }
}

export default Calculator;