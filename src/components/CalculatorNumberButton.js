import React from "react";
class CalculatorNumberButton extends React.Component{
    render() {
        return (
            <button onClick={() => {
                this.props.function(this.props.value)
              }}>{this.props.value}</button>
        );
    }
}

export default CalculatorNumberButton;